FROM alpine
COPY _opt /opt/lgsvl
VOLUME /opt/lgsvl
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
