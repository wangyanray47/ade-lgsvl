## ADE LGSVL

- This project generates an LGSVL (https://github.com/lgsvl/simulator) volume for [`ade`](https://gitlab.com/ApexAI/ade-cli)

### How to use

- In the `.aderc` file add, `registry.gitlab.com/apexai/ade-lgsvl:latest` to the list of `ADE_IMAGES`: e.g.

```
export ADE_IMAGES="
  registry.gitlab.com/group/project/ade:latest
  registry.gitlab.com/apexai/ade-lgsvl:latest
"
```

### Getting a specific LGSVL version

- Generating a volume for a different LGSVL version is as simple as creating a git tag (e.g `2019.11`)
- [Create an issue](https://gitlab.com/ApexAI/ade-lgsvl/issues/new), if the desired version is not available 
